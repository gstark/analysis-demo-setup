open -a XQuartz 
xhost + 127.0.0.1
docker run --rm -it -v $PWD:$PWD -w $PWD -e WORK=$PWD -e DISPLAY=host.docker.internal:0 atlas/analysisbase:22.2.0 bash --init-file ./profile.sh
